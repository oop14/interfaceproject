/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceproject;

/**
 *
 * @author Aritsu
 */
public class Plane extends Vehicle implements Flyable,Runable{

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: Start Enging !");
    }

    @Override
    public void stopEngine() {
        
    }

    @Override
    public void raiseSpeed() {
       
    }

    @Override
    public void applyBreak() {
        
    }

    @Override
    public void fly() {
        System.out.println("Plane  fly!!");
    }

    @Override
    public void run() {
       System.out.println("Plane  run!!");
    }
    
}
