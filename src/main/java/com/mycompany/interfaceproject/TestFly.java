/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceproject;

/**
 *
 * @author Aritsu
 */
public class TestFly {
    public static void main(String[] args) {
        Bat bat1 = new Bat("Baty");
        Plane plane1 = new Plane("Engine112");
        Dog dog1 = new Dog("Dogy");
        bat1.fly();
        plane1.fly();
        
        Flyable[] flyables ={bat1,plane1};
        for(Flyable f : flyables) {
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        Runable[] runables = {dog1,plane1};
        for(Runable r: runables){
            if(r instanceof Plane){
                Plane p = (Plane)r;
                p.startEngine();
            }
            r.run();
        }
    }
}
